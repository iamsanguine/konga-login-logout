﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Auto_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver browser = new FirefoxDriver();
            browser.Navigate().GoToUrl("https://www.konga.com");
            browser.FindElement(By.XPath("/html/body/div[5]/header/div[1]/div/div/div/div[2]/div/a[2]")).Click();
            browser.FindElement(By.XPath("//*[@id='username']")).SendKeys("Insert Email");
            Thread.Sleep(3000);
            browser.FindElement(By.XPath("//*[@id='pass']")).SendKeys("Insert Password");
            browser.FindElement(By.XPath("//*[@id='send2']")).Click();
            Thread.Sleep(2000);
            browser.FindElement(By.XPath("//*[@id='search']")).SendKeys("sneakers");
            browser.FindElement(By.XPath("/html/body/div[5]/header/div[2]/div[2]/div[2]/div/div[1]/div[1]/form/div/div[1]")).Click();

            /*Trying to hover to show log out feature*/
            browser.FindElement(By.XPath("//*[@id='loginButton']")).Click();
            Thread.Sleep(3000);
            IWebElement drop = browser.FindElement(By.XPath("/html/body/header/div[1]/div/div/div/div[2]/div/a/span[2]"));
            Actions move = new Actions(browser);
            move.MoveToElement(drop).Perform();

            browser.FindElement(By.XPath("/html/body/header/div[1]/div/div/div/div[2]/div/div/ul/li[5]/a")).Click();
            browser.Quit();
        }
    }
}
